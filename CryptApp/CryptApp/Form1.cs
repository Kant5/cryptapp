﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CryptoApp
{
    public partial class CryptoForm : Form
    {
        private string IncomingText;
        private string OutComingText;
        public CryptoForm()
        {
            InitializeComponent();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Enabled = false;
            textBox3.Clear();
            textBox1.Enabled = true;
            btnOpen1.Enabled = true;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            textBox3.Enabled = true;
            IncomingText = "";
            textBox1.Clear();
            textBox1.Enabled = false;
            btnOpen1.Enabled = false;
        }
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == 0)
            {
                textBox4.Clear();
                textBox9.Clear();
                IncomingText = "";
                radioButton6.Checked = true;
                numericUpDown2.Value = 1;
                btnSave1.Enabled = false;
            }
            else
            {
                radioButton1.Checked = true;
                radioButton3.Checked = true;
                textBox2.Clear();
                textBox3.Clear();
                IncomingText = "";
                OutComingText = "";
                numericUpDown1.Value = 1;
                btnSave2.Enabled = false;
            }
        }
        /// <summary>
        /// Метод SaveFile() проверяет
        /// правильность заполнении формы
        /// </summary>
        /// <param name="flag">Флаг шифрования</param>
        private bool CheckError(bool flag)
        {
            if (flag)
            {
                if (radioButton1.Checked && textBox3.Text == "")
                {
                    MessageBox.Show("Не введен текст для шифрования!", "Не введен текст", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else if (radioButton2.Checked && textBox1.Text == "")
                {
                    MessageBox.Show("Не добавлен файл с текстом для шифрования!", "Не добавлен файл", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else if (radioButton2.Checked && IncomingText.Length == 0)
                {
                    MessageBox.Show("Нет текста в добавленном файле!", "Нет текста", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else return false;
            }
            else
            {
                if (textBox4.Text == "")
                {
                    MessageBox.Show("Не добавлен файл с текстом для дешифрования!", "Не добавлен файл", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else if (IncomingText.Length == 0)
                {
                    MessageBox.Show("Нет текста в добавленном файле!", "Нет текста", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return true;
                }
                else return false;
            }
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            textBox2.Clear();
            bool error = CheckError(true);//Проверка заполнения формы
            if (!error)
            {
                bool diraction;//напровление шага true - направо; false - налево
                if (radioButton3.Checked) diraction = true;
                else diraction = false;
                if (radioButton1.Checked) IncomingText = textBox3.Text;//получение введенного текста
                textBox2.Text += "Текст шифруется...\r\n";
                OutComingText = CipherCaesar.ConversionText(IncomingText, diraction, (int)numericUpDown1.Value);
                textBox2.Text += "Зашифрованный текст: " + OutComingText + "\r\n";
                btnSave1.Enabled = true;
            }

        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            textBox9.Clear();
            bool error = CheckError(false);
            if (!error)
            {
                bool diraction;//напровление шага true - направо; false - налево
                if (radioButton6.Checked) diraction = true;
                else diraction = false;
                textBox9.Text += "Текст расшифровывается...\r\n";
                OutComingText = CipherCaesar.ConversionText(IncomingText, diraction, (int)numericUpDown1.Value);
                textBox9.Text += "Расшифрованный текст: " + OutComingText + "\r\n";
                btnSave2.Enabled = true;
            }
        }
        /// <summary>
        /// Метод SaveFile() сохраняет 
        /// текст в файле с указанным путем 
        /// и расширением файла
        /// </summary>
        /// <param name="fileName">Полный путь к файлу</param>
        private void SaveFile(string fileName)
        {
                if (fileName.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase))
                {
                    //создается текстовый файл и записывается текст
                    File.WriteAllText(fileName, OutComingText);
                }
                else
                {
                    try
                    {
                        //создание документа Word
                        using (WordprocessingDocument doc = WordprocessingDocument.Create(fileName, DocumentFormat.OpenXml.WordprocessingDocumentType.Document))
                        {
                            //добвляем главную часть документа
                            MainDocumentPart mainPart = doc.AddMainDocumentPart();
                            // Создание структуры документа и добавление текста
                            mainPart.Document = new Document();
                            Body body = mainPart.Document.AppendChild(new Body());
                            Paragraph para = body.AppendChild(new Paragraph());
                            Run run = para.AppendChild(new Run());
                            run.AppendChild(new Text(OutComingText));//добавление текста
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
        }

        private void btnSave1_Click(object sender, EventArgs e)
        {
            SaveDialog(true, "Сохрание зашифрованного текста в файл");
        }

        private void btnSave2_Click(object sender, EventArgs e)
        {
            SaveDialog(false, "Сохрание расшифрованного текста в файл");
        }
        /// <summary>
        /// Метод SaveDialog() открывает 
        /// форму SaveFileDialog и сохраняет 
        /// текст в файл с выбранной директорией
        /// </summary>
        /// <param name="flag">Флаг шифрования</param>
        /// <param name="title">Заголовок в форме SaveFileDialog</param>
        private void SaveDialog(bool flag, string title)
        {
            saveFileDialog.Title = title;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (flag)
                {
                    textBox2.Text += "Зашифрованный текст сохранен в файле: " + saveFileDialog.FileName + "\r\n";
                    SaveFile(saveFileDialog.FileName);
                }
                else
                {
                    textBox9.Text += "Расшифрованный текст сохранен в файле: " + saveFileDialog.FileName + "\r\n";
                    SaveFile(saveFileDialog.FileName);
                }
            }
        }

        private void saveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            if (!saveFileDialog.FileName.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase) && !saveFileDialog.FileName.EndsWith(".docx", StringComparison.InvariantCultureIgnoreCase))
            {
                MessageBox.Show("Выберите или введите файл с расширением '.txt' или '.docx'", "Неверное расширение файла", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }

        private void btnOpen1_Click(object sender, EventArgs e)
        {
            OpenDialog(true);
        }

        private void btnOpen2_Click(object sender, EventArgs e)
        {
            OpenDialog(false);
        }
        /// <summary>
        /// Метод OpenDialog() открывает
        /// текстовый файл или документ Word
        /// при шифрации или дешифрации
        /// </summary>
        /// <param name="flag">Флаг шифрования</param>
        private void OpenDialog(bool flag)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialog.FileName.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase))
                {
                    if (flag) textBox1.Text = openFileDialog.FileName;
                    else textBox4.Text = openFileDialog.FileName;
                    IncomingText = ReadAllText(openFileDialog.FileName);//изъятие текста из текстового файла с определенной кодировкой
                }
                else if (openFileDialog.FileName.EndsWith(".docx", StringComparison.InvariantCultureIgnoreCase))
                {
                    try
                    {
                        if (flag) textBox1.Text = openFileDialog.FileName;
                        else textBox4.Text = openFileDialog.FileName;
                        //изъятие текста из документа Word
                        using (WordprocessingDocument doc = WordprocessingDocument.Open(openFileDialog.FileName, false))
                        {
                            //извлечение текста из файла
                            IncomingText = doc.MainDocumentPart.Document.InnerText;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        IncomingText = null;
                    }
                }
            }
            else textBox1.Text = "";
        }
        /// <summary>
        /// Метод ReadAllText() определяет кодировку
        /// текстового файла и считывает текст в
        /// определенной кодировке
        /// </summary>
        /// <param name="path">Полный путь к текстовому файлу</param>
        /// <returns>Извлеченный текст из файла</returns>
        private string ReadAllText(string path)
        {
            var detector = new Ude.CharsetDetector();
            try
            {
                //определение кодировки текстового файла
                using (FileStream fs = File.OpenRead(path))
                {
                    byte[] bytes = new byte[4000];
                    int bytesRead = fs.Read(bytes, 0, bytes.Length);
                    detector.Feed(bytes, 0, bytesRead);
                    detector.DataEnd();
                }
                return File.ReadAllText(path, Encoding.GetEncoding(detector.Charset));//изъятие текста из файла
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            if (!openFileDialog.FileName.EndsWith(".txt", StringComparison.InvariantCultureIgnoreCase) && !openFileDialog.FileName.EndsWith(".docx", StringComparison.InvariantCultureIgnoreCase))
            {
                MessageBox.Show("Выберите файл с расширением '.txt' или '.docx'", "Неверное расширение файла", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                e.Cancel = true;
            }
        }
    }
}
