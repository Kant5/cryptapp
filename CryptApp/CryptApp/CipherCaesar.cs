﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CryptoApp
{
    /// <summary>
    ///  Класс CipherCaesar
    ///  позволяет преобразовывать текст
    ///  путем сдвига по алфовиту (Шифр Цезаря)
    /// </summary>
    public class CipherCaesar
    {
        private static string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";

        /// <summary>
        /// Метод ShiftLetter() сдвигает
        /// исходный символ по алфовиту 
        /// с указаными направлением и шагом сдвига
        /// </summary>
        /// <param name="letter">Исходный символ</param>
        /// <param name="direction">Направление сдвига</param>
        /// <param name="step">Шаг сдвига</param>
        /// <returns>Измененный символ</returns>
        private static char ShiftLetter(char letter, bool direction, int step)
        {
            int index;//индекс переданного символа в alphabet
            bool isLetter = Regex.IsMatch(letter.ToString(), @"[а-яёА-ЯЁ]");// вхождение переданного символа в алфавит
            bool register = Regex.IsMatch(letter.ToString(), @"[а-яё]") ? false : true; //определение регистра переданного символа
            if (isLetter)
            {
                if (register) letter = char.ToLower(letter);
                index = Array.IndexOf(alphabet.ToCharArray(), letter);
                //получение нового индекса в записимости от шага и направления
                if (direction) index = (index + step) > alphabet.Length - 1 ? Math.Abs(33 - (index + step)) : index + step;
                else index = (index - step) < 0 ? 33 + (index - step) : index - step;
                letter = alphabet[index];
                if (register) return char.ToUpper(letter);
                else return letter;
            }
            else return letter;
        }

        /// <summary>
        /// Метод ConversionText() преобразует
        /// путем сдвига по алфовиту (Шифр Цезаря)
        /// </summary>
        /// <param name="text">Исходный текст</param>
        /// <param name="direction">Направление сдвига</param>
        /// <param name="step">Шаг сдвига</param>
        /// <returns>Измененный текст</returns>
        public static string ConversionText(string text, bool direction, int step)
        {
            if (text.Length != 0)
            {
                StringBuilder sb = new StringBuilder(text.Length);
                for (int index = 0; index < text.Length; index++)
                {
                    sb.Append(ShiftLetter(text[index], direction, step));
                }
                return sb.ToString();
            }
            else return null;
        }
    }
}
