﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CryptoApp;

namespace TestCryptApp
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestConversionText()
        {
            //Arrage
            string text = "Вам!";
            bool direction = false;
            int step = 0;
            string result;
            //Act
            result = CipherCaesar.ConversionText(text, direction, step);
            //Assert
            Assert.AreEqual("Вам!", result);
        }

        [TestMethod]
        public void TestConversionText_Step2()
        {
            //Arrage
            string text = "Вам,Hello]}?#125!";
            bool direction = true;
            int step = 2;
            string result;
            //Act
            result = CipherCaesar.ConversionText(text, direction, step);
            //Assert
            Assert.AreEqual("Дво,Hello]}?#125!", result);
        }

        [TestMethod]
        public void TestConversionText_ENText()
        {
            //Arrage
            string text = "Hello!";
            bool direction = true;
            int step = 2;
            string result;
            //Act
            result = CipherCaesar.ConversionText(text, direction, step);
            //Assert
            Assert.AreEqual("Hello!", result);
        }
    }
}
